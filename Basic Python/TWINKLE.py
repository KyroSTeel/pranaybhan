import RPi.GPIO as GPIO
import time
import sys,tty,termios
GPIO.setmode(GPIO.BCM)
List1=[3,2,15,14]
GPIO.setup(List1,GPIO.OUT)
ra=3
rb=2
la=14
lb=15
def Percy():
    GPIO.output(ra,GPIO.HIGH)#Forward1
    GPIO.output(la,GPIO.HIGH)#Forward2
    GPIO.output(lb,GPIO.LOW)#Forward1
    GPIO.output(rb,GPIO.LOW)#Forward2
def Nico():
    GPIO.output(rb,GPIO.HIGH)
    GPIO.output(lb,GPIO.HIGH)
    GPIO.output(la,GPIO.LOW)
    GPIO.output(ra,GPIO.LOW)
def Leo():
    GPIO.output(la,GPIO.HIGH)
    GPIO.output(lb,GPIO.LOW)
    GPIO.output(ra,GPIO.LOW)
    GPIO.output(rb,GPIO.LOW)
def DarthVader():
    GPIO.output(ra,GPIO.HIGH)
    GPIO.output(rb,GPIO.LOW)
    GPIO.output(la,GPIO.LOW)
    GPIO.output(lb,GPIO.LOW)
def Yoda():
    GPIO.output(ra,GPIO.LOW)
    GPIO.output(rb,GPIO.LOW)
    GPIO.output(la,GPIO.LOW)
    GPIO.output(lb,GPIO.LOW)
def getch():
    fd=sys.stdin.fileno()
    old_settings=termios.tcgetattr(fd)
    try:
        tty.setraw(sys.stdin.fileno())
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd,termios.TCSADRAIN,old_settings)
    return ch

while True:
    print("Command...")
    if getch() == "w":
        Percy()
        print("w")
        input()
    if getch() == "s":
        Nico()
        print("s")
        input()
    if getch() == "a":
        Leo()
        print("a")
        input()
    if getch() == "d":
        DarthVader()
        print("d")
        input()
    if getch() == "r":
        Yoda()
        print("Shutting down....")
        input()
    if getch() == "x":
        GPIO.cleanup()
    
    


