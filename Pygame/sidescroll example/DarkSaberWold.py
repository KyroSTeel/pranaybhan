#create animation for walk(l or r)
#create background
#scroll background
import pygame
from pygame.locals import*
import random
import time
pygame.init()
screen = pygame.display.set_mode((640,480))
white=(255,255,255)
class DarkSaberWolf:
    def __init__(self):
        self.idle=[]
        for s in range(1,101):
            image=pygame.image.load("./DarkSaber/idle/darksaber_stand00"+str(s)+".png")
            image=pygame.transform.rotozoom(image,0,.4)
            self.idle.append(image)
        self.attack=[]
        for r in range(1,78):
            image=pygame.image.load("./DarkSaber/attack/darksaber_attack00"+str(r)+".png")
            image=pygame.transform.rotozoom(image,0,.4)
            self.attack.append(image)
        self.walk=[]
        for f in range(1,39):
            image=pygame.image.load("./DarkSaber/walk/darksaber_walk00"+str(f)+".png")
            image=pygame.transform.rotozoom(image,0,.4)
            self.walk.append(image)
        self.x=320
        self.y=240
        self.count=0
        self.direction="right"
        self.atk=0
        self.atkcnt=0
        self.wlk=0
        self.wlkcnt=0
    def update(self):
        if self.atk==1:
        #ATTACK
            self.atkcnt=self.atkcnt+1
            if self.atkcnt >= 77:
                self.atkcnt=0
                self.atk=0
            if self.direction=="right":
                screen.blit(self.attack[self.atkcnt],(self.x-100,self.y-38))
            if self.direction=="left":
                screen.blit((pygame.transform.flip(self.attack[self.atkcnt],1,0)),(self.x-180,self.y-38))
        #WALK
        elif self.wlk==1:
            self.wlkcnt = self.wlkcnt+1
            if self.wlkcnt >=38:
                self.wlkcnt=0
            if self.direction=="right":
                screen.blit(self.walk[self.wlkcnt],(self.x-40,self.y-20))
                self.x=self.x+3
            

            if self.direction=="left":
                screen.blit((pygame.transform.flip(self.walk[self.wlkcnt],1,0)),(self.x,self.y-20))
                self.x=self.x-3
        #IDLE
        else:
            self.count=self.count+1
            if self.count >= 99:
                self.count=0
            #next 4 lines deal with switching position of sprite, by flipping it vertically
            if self.direction=="left":
                screen.blit((pygame.transform.flip(self.idle[self.count],1,0)),(self.x,self.y))
            if self.direction=="right":
                screen.blit(self.idle[self.count],(self.x,self.y))
            
    
dsw=DarkSaberWolf()
while True:
    screen.fill(white)
    dsw.update()
    pygame.display.update()
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            exit()
        if event.type == KEYDOWN:
            if event.key == K_LEFT:
                dsw.direction="left"
                dsw.wlk=1
                dsw.x=dsw.x-10
            if event.key == K_RIGHT:
                dsw.direction="right"
                dsw.wlk=1
                dsw.x=dsw.x+10
            if event.key == K_n:
                dsw.atk=1
        if event.type == KEYUP:
            if event.key == K_LEFT:
                dsw.wlk=0
                dsw.wlkcnt=0
            if event.key == K_RIGHT:
                dsw.wlk=0
                dsw.wlkcnt=0
            
                
                
        
            
    
    
