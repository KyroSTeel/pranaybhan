import pygame
from pygame.locals import*
import random
import time
import sqlite3
from tkinter import*
from tkun import*
def Game():
    pygame.init()
    screen = pygame.display.set_mode((640,480))
    red=(255,0,0)
    white=(255,255,255)
    green=((0,255,0))
    black=(0,0,0)
    left=0
    right=0
    counter=0
    bulletc=42-counter
    chance=random.randint(0,bulletc)
    escapees=0
    bltspd=8
    ResCount=0
    hitbox=20
    Blue=(0,0,225)
    rounder=1
    class SpaceCraft:
        def __init__(self,x,y):
            self.gametime=pygame.time.get_ticks()/1000
            self.conn = sqlite3.connect('highscore.db')
            self.cur=self.conn.cursor()
            self.cur.execute("create table if not exists Highscores (High integer)")
            self.x=x
            self.y=y
            self.bx=x
            self.by=y
            self.now=time.time()
            self.left=0
            self.right=0
            self.shoot=0
            self.bullet=pygame.image.load("the_bullet.png")
            self.image=pygame.image.load("Main.png")
            self.image=pygame.transform.scale(self.image,(45,45))
            self.bullet=pygame.transform.scale(self.bullet,(10,10))
        def update(self):
            screen.blit(self.image,[self.x,self.y])
            if self.left==1:
                self.x=self.x-10
            if self.right==1:
                self.x=self.x+10
            if self.shoot==1:
                screen.blit(self.bullet,[self.bx,self.by])
                self.by=self.by-10
                if self.by<=0:
                    self.shoot=0
            else:
                self.by=self.y
                self.bx=self.x
        def updb(self):
            query="insert into Highscores (High) values("+str(self.gametime)+")"
            print(query)
            self.cur.execute(query)
        def rddb(self):
            print("Hi2")
            dbinfo=self.cur.execute("select * from Highscores")
            for i in dbinfo.fetchall():
                print(i)
    class Alien:
        def __init__(self,x,y):
            self.x=x
            self.y=y
            self.bx=x
            self.by=y
            self.chance=0
            self.image=pygame.image.load("Invader1.png")
            self.image=pygame.transform.scale(self.image,(25,25))
            self.bullet=pygame.image.load("the_bullet.png")
            self.bullet=pygame.transform.scale(self.bullet,(20,20))
            self.timing=random.randint(100,300)
            self.t=0
        def update(self):
            screen.blit(self.image,[self.x,self.y])
            self.x=self.x+random.randint(-1,1)
            self.y=self.y+random.randint(-1,1)
            if self.chance ==1 :
                screen.blit(self.bullet,[self.bx,self.by])
                self.by=self.by+bltspd
                if self.by>=640:
                    self.by=self.y
                    self.chance=0
            else:
                self.t=self.t+1
                if self.t==self.timing:
                    self.chance=1
                    self.t=0
            
    class Spec:
        def __init__(self,x,y):
            self.image=pygame.image.load("SpecialInvader.png")
            self.image=pygame.transform.scale(self.image,(20,10))
            self.x=x
            self.y=y
        def update(self):
            screen.blit(self.image,[self.x,self.y])
            if self.x < 640:
                self.x=self.x+2
            else:
                self.x=-20
            
                    
                    
                    
    spec=Spec(100,125)
    alienlist=[]
    for x in range(15,625,45):
        for y in range(15,120,45):
            alien=Alien(x,y)
            alienlist.append(alien)
            timing=random.randint(1,10)
                   
    spacecraft=SpaceCraft(300,420)
    def show_text(msg,x,y,color):
        fontobj= pygame.font.SysFont("freesans",32)
        msgobj= fontobj.render(msg,False,color)
        screen.blit(msgobj,(x,y))
        
    clock=pygame.time.Clock()


    while True:
        screen.fill((0,0,0))
        gametime=pygame.time.get_ticks()/1000
        '''print(gametime)'''
        clock.tick(200)
        show_text("Speed:"+str(bltspd) +"Hitbox:"+str(hitbox)+"Score:"+str(counter*50)+"Wave:"+str(rounder),0,440,Blue)
        for a in alienlist:
            if a.y<=spacecraft.by<=a.y+hitbox and a.x<=spacecraft.bx<=a.x+hitbox:
                counter=counter+1
                (counter,"/42 Dead, hit 'em all!")
                spacecraft.shoot=0
                alienlist.remove(a)
            if spacecraft.x<=a.bx<=spacecraft.x+10 and spacecraft.y<=a.by<=spacecraft.by+10:
                spacecraft.updb()
                spacecraft.rddb()
                spacecraft.conn.commit()
                spacecraft.conn.close()
                print("You Died")
                pygame.display.quit()
                pygame.quit()
                Call()
                Spacecraft.rddb()
            if spec.x<=spacecraft.bx<=spec.x+10 and spec.y<=spacecraft.by<=spec.y+10:
                spec.x=-20
                print("Noble Down!")
                hitbox=hitbox*2
                counter=counter+200
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_n:
                    spacecraft.left=1
                if event.key == K_m:
                    spacecraft.right=1
                if event.key == K_SPACE:
                    if spacecraft.shoot==0:
                        spacecraft.shoot=1
                        spacecraft.bx=spacecraft.x
            if event.type == KEYUP:
                if event.key == K_SPACE:
                    spacecraft.shoot=0
                    

                if event.key == K_n:
                    spacecraft.left=0
                if event.key == K_m:

                    spacecraft.right=0
            
            if event.type == QUIT:
                spacecraft.updb()
                spacecraft.rddb()
                pygame.quit()
                exit()
        spacecraft.update()
        spec.update()
        for alien in alienlist:
            alien.update()
        for a in alienlist:
            if a.x ==640 or a.x==0:
                alienlist.remove(a)
                escapees=escapees+1
                #print(escapees,"Escaped")
                x=random.randint(15,640)
                y=random.randint(15,120)
                alien=Alien(x,y)
                alienlist.append(alien)
            if a.y==480 or a.y==0:
                escapees=escapees+1
                #print(escapees,"Escaped")
                alienlist.remove(a)
                x=random.randint(15,640)
                y=random.randint(15,120)
                alien=Alien(x,y)
                alienlist.append(alien)
                
        if len(alienlist) == 0:
            for x in range(15,625,45):
                for y in range(15,120,45):
                    alien=Alien(x,y)
                    alienlist.append(alien)
                    timing=random.randint(1,10)
            rounder=rounder+1
            bltspd=bltspd+(2*rounder)
            print(bltspd)
     # Whenever the blt is higher than or equal to 5,
     #the computer actually prioritizes the bullets that are closest to the player,
     #making the bullets closest fire faster               
            
        if escapees == 45:
            print("``THEY ALL ESCAPED!!``\n``GE- -UT OF T--RE N-W!! T-EY BR--CHED OU- DE-EN-ES``\n[Sound of gunshot]\n``We come for you,mortal``")
            spacecraft.updb()
            print("ded")
            pygame.display.quit()
            pygame.quit()
            Call()
            spacecraft.rddb()
            spacecraft.conn.commit()
            spacecraft.conn.close()
        if spacecraft.x>=610 or spacecraft.x<=0:
            spacecraft.x=275
        pygame.display.flip()
        
        
        
