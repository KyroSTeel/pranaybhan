import pygame
from pygame.locals import*
import random
import time
import tkinter
#Initialization
OnePunch=tkinter.Tk()
pygame.init()
screen = pygame.display.set_mode((640,480))
#Stats
dmgr=100

dmgb=10

hitred=100

hitblue=200

MaxHlth=100

Speed=2

Regen=0

Crit=100
#Stat Decision
def Speedy():
    global Speed
    Speed=Speed-0.1
    print("Increase S")
def Colossus():
    global dmgr
    dmgr=dmgr+2
    print("Increase P")
def Smoker():
    global Crit
    Crit=Crit-2
    print("Increase C")
def DeadPool():
    global Regen
    Regen=Regen+0.001
    print("Increase R")
def Tank():
    global hitred
    global MaxHlth
    hitred=hitred+5
    MaxHlth=MaxHlth+5
    print("Increase H")

exp=0
lvl=1
#Tk Widgets
LvlStat=tkinter.Frame()
Label=tkinter.Label(LvlStat,text="Stat Upgrade")
SpeedB=tkinter.Button(LvlStat,text="Speed",command=Speedy)
IntelB=tkinter.Button(LvlStat,text="Intel(Crit)",command=Smoker)
PowerB=tkinter.Button(LvlStat,text="Power",command=Colossus)
RegenB=tkinter.Button(LvlStat,text="Regen",command=DeadPool)
HealthB=tkinter.Button(LvlStat,text="Health",command=Tank)
Label.grid(row=0,column=0)
SpeedB.grid(row=1,column=0)
IntelB.grid(row=1,column=1)
PowerB.grid(row=1,column=2)
RegenB.grid(row=2,column=0)
HealthB.grid(row=2,column=1)
LvlStat.pack()
# Counters
Blur=0
BlurCount=10
SpaceCount=0
# Colors
MyPurple=(125,0,255)
Dummy=(182, 155, 76)
# Coor.
t = 300
r = 240
u=30
i=345
o=240
p=30
m=3
n=3
def show_text(msg,x,y,color):
    fontobj= pygame.font.SysFont("freesans",32)
    msgobj= fontobj.render(msg,False,color)
    screen.blit(msgobj,(x,y))
print("This is One Punch Simulator, A custom game made by Shado200Eon,the point is to become as strong as you can, and get to the point you can defeat anyone in one punch. The Dummy will attack as well, at it's own power, and until you can kill it in less than 5 punches, it will stay there. There are infinite amounts. The Stats are Speed, Regen, Health, and Strength. Along with with Intel, which increases critical chance, these stats will be manually upgraded each round.")

while True:
    #Drawing
    OnePunch.update()
    show_text("R:"+str(hitred),2,2,MyPurple)
    show_text("B:"+str(hitblue),2,34,Dummy)
    pygame.draw.rect(screen,Dummy,(i,o,p,p))
    pygame.draw.rect(screen,MyPurple,(t,r,u,u))
    pygame.display.update()
    screen.fill((255,255,255))
    #Regen
    Regener=random.randint(1,2)
    if Regener == 2:
        hitred=hitred + (Regen * 0.25)
        if hitred > MaxHlth:
            hitred=hitred
    #Event Loop
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            exit()
        if event.type == KEYDOWN:
            if event.key == K_SPACE:
                Blur=1
                exp=exp+50
                hitblue=hitblue-dmgr
                print("-",dmgr)
                #pygame.display.update()
                #SpaceCount=SpaceCount+1
                print(exp,lvl)
    #Exp for Kill
    if hitblue <= 0:
        hitblue=hitblue+(300+(lvl*10))
        exp=exp+25
    #Regen for Dummy
    if hitred == 0:
        hitblue=200
    #Level Up Initialization
    if exp >= 100:
        lvl=lvl+1
        exp=0
    #Blur Movement
    if Blur:
        BlurCount=BlurCount-1
        if 5<BlurCount<10:
            t=t+3
        if 0<BlurCount<5:
            t=t-3
        if BlurCount==0:
            BlurCount=10
            Blur=0
            time.sleep(Speed)
        
        
