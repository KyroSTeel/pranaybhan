from flask import Flask, render_template, request, redirect, flash, session
from flask_pymongo import PyMongo
from bson import ObjectId
app=Flask("OneStopShop")
app.config["SECRET_KEY"]="asdfgh"
app.config["MONGO_URI"]="mongodb+srv://pranay:nfXO5cSvUR6OZHyo@cluster0-xaw3y.mongodb.net/onestopshop_db?retryWrites=true&w=majority"
pymongo=PyMongo(app)
@app.route("/", methods=["GET","POST"])
def index():
    session['cart-items'] = {}
    return render_template("index.html")
@app.route("/add", methods=["GET", "POST"])
def add():
    if request.method == "GET":
        return render_template('add.html')
    elif request.method == "POST":
        doc={}
        for item in request.form:
            doc[item] = request.form[item]
        pymongo.db.products.insert_one(doc)
    return render_template("Add.html")
@app.route("/buy", methods=["GET", "POST"])
def buy():
    if request.method=="GET":
        found_products = pymongo.db.products.find()
        return render_template('buy.html', products=found_products)
    if request.method == "POST":
        itemid=request.form["buy"]
        itemquantity=request.form["quantity"]
        pastitems=pymongo.db.cart.find_one({"itemid":itemid})
        if pastitems==None:
            pymongo.db.cart.insert_one({"itemid": itemid, "itemquantity": itemquantity})
        else:
            newitem=int(pastitems["itemquantity"])+int(itemquantity)
            pymongo.db.cart.update_one({"itemid":itemid},{"$set":{"itemquantity":str(newitem)}})
        buying=pymongo.db.products.find_one({"_id":ObjectId(itemid)})
        remainder=int(buying["quantity"])-int(itemquantity)
        if remainder<=0:
            flash("Unable | Out of Stock")
            remainder=int(buying["quantity"])
        pymongo.db.products.update_one({"_id":ObjectId(itemid)},{"$set":{"quantity":str(remainder)}})
        print(remainder)
        print(session)
        return redirect('/buy')
@app.route("/checkout", methods=["GET", "POST"])
def checkout():
    if request.method=="GET":
        found_cart = pymongo.db.cart.find()
        d={}
        for item in found_cart:
            temp=pymongo.db.products.find_one({"_id":ObjectId(item["itemid"])})
            d[item["itemid"]]={"quantity":item["itemquantity"],"desc":temp["desc"],"price":temp["price"],"name":temp["title"]}
        return render_template("checkout.html", cart=d)
    if request.method=="POST":
        if request.method == "POST":
            print(request.form)


            return redirect("/home")
        return redirect('/checkout')


app.run(debug=True)