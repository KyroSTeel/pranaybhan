from flask import Flask, render_template, request, redirect, flash, session
from flask_pymongo import PyMongo
app=Flask("KeepTrack")
app.config["SECRET_KEY"]="asdfgh"
app.config["MONGO_URI"]="mongodb+srv://pranay:nfXO5cSvUR6OZHyo@cluster0-xaw3y.mongodb.net/keeptrack_db?retryWrites=true&w=majority"
pymongo=PyMongo(app)
@app.route("/", methods=["GET","POST"])
def index():
    if "user" in session:
        flash("Already logged in!", "success")
        return redirect("/home")
    if request.method=="POST":
        print(request.form)
        if "username" not in request.form or request.form["username"] == "":
            flash("username is required","primary")
            return redirect("/")
        if "password" not in request.form or request.form["password"] == "":
            flash("password is required","warning")
            return redirect("/")
        user = pymongo.db.accounts.find_one({"username": request.form["username"]})
        if user is None:
            flash("User does not exist, please register.", "danger")
            return redirect("/Register")
        if request.form["password"] != user["password"]:
            flash("Incorrect password.", "danger")
            return redirect("/")
        session["user"]=request.form["username"]
        return redirect("/home")
    else:
        return render_template("Login.html")
@app.route("/Register", methods=["GET","POST"])
def register():
    if request.method=="POST":
        print(request.form)
        if "username" not in request.form or request.form["username"] == "":
            flash("username is required","primary")
            return redirect("/Register")
        if "password" not in request.form or request.form["password"] == "":
            flash("password is required","warning")
            return redirect("/Register")
        if "confirm_password" not in request.form or request.form["confirm_password"] == "":
            flash("reconfirm password", "warning")
            return redirect("/Register")
        user = pymongo.db.accounts.find_one({"username":request.form["username"]})
        if user is not None:
            flash("User already exists", "danger")
            return redirect("/Register")
        if request.form["password"] != request.form["confirm_password"]:
            flash("Passwords don't match", "danger")
            return redirect("/Register")
        pymongo.db.accounts.insert_one({"username":request.form["username"],"password":request.form["password"]})
        #if username and password in request.form:
        flash("Account successfully made, please contine to login.", "success")
        return redirect("/")
    else:
        return render_template("Register.html")
@app.route("/home", methods=["GET","POST"])
def home():
    if "user" not in session:
        flash("You need to sign in to access this page", "danger")
        return redirect("/")
    if request.method=="POST":
        print(request.form)
        if "delete" in request.form:
            pymongo.db.project.update_one({"goaltext":request.form["delete"]},{"$set":{"status":"dismissed"}})
        elif "archive" in request.form:
            pymongo.db.project.update_one({"goaltext":request.form["archive"]},{"$set":{"status":"archived"}})
        elif "addcard" in request.form:
            goaltext=request.form["goaltext"]
            date=request.form["Deadline"]
            goaltime = request.form["goaltime"]
            account = session["user"]
            pymongo.db.project.insert_one({"goaltext":goaltext,"goaltime":goaltime, "date":date, "account":account, "status": "in_progress"})

        return redirect("/home")


    if request.method=="GET":
        goal=pymongo.db.project.find({"account":session["user"],"status":"in_progress"})
        ncgoals=pymongo.db.project.find({"account":session["user"],"status":"dismissed"})
        return render_template("Home.html",goal=goal,ncgoals=ncgoals)
@app.route("/logout")
def logout():
    session.clear()
    return redirect("/")
app.run(debug=True)

